package kanxoramesh.myalarm.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import kanxoramesh.myalarm.MainActivity;
import kanxoramesh.myalarm.R;

/**
 * Created by Ramesh on 12/19/2017.
 */

public class RingToneService extends Service {
    MediaPlayer mediaPlayer;
    int startID=0;
    Boolean isRun=false;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int sid) {
        Log.e("startId: ",""+sid);
        String check= intent.getExtras().getString("check");

        assert check!=null;
        switch (check) {
            case "on":
                sid = 1;
                break;
            case "off":
                sid = 0;
                break;
            default:
                sid = 0;
                break;
        }
        if(!isRun && sid==1)
        {
            mediaPlayer= MediaPlayer.create(this, R.raw.plane);
            mediaPlayer.start();
            isRun=true;
            startID=0;
            Intent mainInt=new Intent(this.getApplicationContext(),MainActivity.class);
            PendingIntent mainPendin= PendingIntent.getActivity(this,0,mainInt,0);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setContentTitle("click me")
                    .setContentText("click me")
                    .setAutoCancel(true)
                    .setSmallIcon(R.drawable.ic_launcher_background)
                    .setContentTitle("Alarm is going turning Off !")
                    .setContentIntent(mainPendin)
                    .setAutoCancel(true);
            NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
            try {
                notificationManager.notify(0, notificationBuilder.build());

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

        }
        else if(isRun && sid==0)
        {


            mediaPlayer.stop();
            mediaPlayer.reset();
            isRun=false;
            startID=0;
        }
        else if(!isRun && sid==0)
        {
            isRun=false;
            startID=0;
        }
        else if(isRun && sid==1)
        {
            isRun=true;
            startID=1;
        }
        else
        {

        }

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isRun=false;
    }
}
