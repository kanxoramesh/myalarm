package kanxoramesh.myalarm;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

import kanxoramesh.myalarm.receiver.ALarm_Receiver;

public class MainActivity extends AppCompatActivity {

    AlarmManager manager;
    TimePicker alarm_timePicker;
    TextView updateText;
    Context conext;
    Calendar calendar;
    PendingIntent pendingIntent;
    Toolbar toolbar;
    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        conext= this;
        toolbar= findViewById(R.id.toolbarFirst);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Alarm Set !!");
        alarm_timePicker= findViewById(R.id.timePicker);
        manager= (AlarmManager) getSystemService(ALARM_SERVICE);
        updateText= findViewById(R.id.update_text);
        calendar= Calendar.getInstance();
         intent= new Intent(conext,ALarm_Receiver.class);

    }

   // @TargetApi(Build.VERSION_CODES.M)

    public void on(View view) {
        int hour= 0;
        int minute=0;

            calendar.set(Calendar.HOUR_OF_DAY,alarm_timePicker.getCurrentHour());
            calendar.set(Calendar.MINUTE,alarm_timePicker.getCurrentMinute());
            hour = alarm_timePicker.getCurrentHour();
       minute= alarm_timePicker.getCurrentMinute();
        String hour_String= String.valueOf(hour);
        String minute_String =String.valueOf(minute);
        if(hour>12)
        {
            hour_String=String.valueOf(hour-12);
        }
        if(minute<10)
        {
            minute_String="0"+String.valueOf(minute);
        }
        set_alaram_text("Alarm set to "+hour_String+":"+minute_String);
        intent.putExtra("check","on");
        pendingIntent= PendingIntent.getBroadcast(conext,0,intent,PendingIntent.FLAG_UPDATE_CURRENT);
        manager.set(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(),pendingIntent);

    }

    public void off(View view) {
        set_alaram_text("off");
        intent.putExtra("check","on");
        manager.cancel(pendingIntent);
        intent.putExtra("check","off");
        sendBroadcast(intent);
    }

    private void set_alaram_text(String on) {

        updateText.setText(on);
    }
}
