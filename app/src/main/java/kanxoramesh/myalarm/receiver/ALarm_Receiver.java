package kanxoramesh.myalarm.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import kanxoramesh.myalarm.service.RingToneService;

/**
 * Created by Ramesh on 12/19/2017.
 */

public class ALarm_Receiver extends BroadcastReceiver
{
    @Override
    public void onReceive(Context context, Intent intent) {

        String check= intent.getExtras().getString("check");
        Intent ser= new Intent(context,RingToneService.class);
        ser.putExtra("check",check);
        context.startService(ser);
    }
}
